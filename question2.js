// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  // Write your code here
	let freq=[];
	for(let i = 0;i < string.length;i++)
	{

		if (string[i] >= 'a' && string[i] <= 'z')
		{  
			let char = string.charAt(i);
			//console.log(freq[char]);
			if(freq[char] == undefined)
				freq[char] = 1;
			else
				freq[char] += 1;
		}
	}
	console.log(freq);
	return freq;
	 
}