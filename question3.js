let flattenObject = {};
function recursiveFlatten(unflatObject,objectStack)
{
	for(let key in unflatObject)
	{
		console.log(key + " " + unflatObject[key] + " " + typeof(unflatObject[key])) + " "+unflatObject[key].hasOwnProperty(unflatObject) + "\n";
		if(typeof(unflatObject[key]) != "object")
		{

			let keyString = "";
			for(let i = 0;i < objectStack.length;i++)
			{
				keyString += objectStack[i]+".";
			}
			keyString+=key;
			console.log("Key string is " + keyString + " value is " + unflatObject[key]);
			console.log("type of keystring is", typeof(keyString))
			flattenObject["\"" + keyString + "\""] = unflatObject[key];
			console.log(flattenObject);
		}
		else
		{
			objectStack.push(key);
			console.log(key + " pushed to stack");
			recursiveFlatten(unflatObject[key], objectStack);
		}
	}
		if(objectStack.length != 0)
			objectStack.pop();
}
// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
	let objectStack = [];
	recursiveFlatten(unflatObject, objectStack);
	console.log(flattenObject);
	// console.log(recursiveFlatten.constructor.name=="AsyncFunction");
	return flattenObject;
}
