// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(flatObject) {
  let unflatObject = {}, temp;
  for(key in flatObject)
  {
	  let keyString = key.split(".");
	  temp = unflatObject;
	  for(let i = 0; i < keyString.length-1; i++)
	  {
		  if(temp[keyString[i]] == undefined)
		  {
			  if(keyString[i+1] == undefined || typeof(keyString[i+1]) == "Number")
			  	temp[keyString[i]] = [];
			  else	
			  	temp[keyString[i]] = {};
		  }
		temp = temp[keyString[i]];
	  }
	temp[keyString[keyString.length-1]] = flatObject[key];
	console.log((JSON.stringify(unflatObject)));
  }
  return unflatObject;
}