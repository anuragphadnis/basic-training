function max(first, second)
{
	return first > second ? first : second;
}
function min(first, second)
{
	return first < second ? first : second;
}
// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
// Write your code here
	let highest,secondHighest;
	if(array.length==1)
	{
		highest=secondHighest=array[0];
	}
	else if(array.length>1)
	{
		highest=max(array[0],array[1]);
		secondHighest=min(array[0],array[1]);
		for(let i = 2;i < array.length;i++)
		{
			if(array[i] > highest)
			{
				secondHighest = highest;
				highest = array[i];
			}
			else if(array[i]>secondHighest)
			secondHighest = array[i];
		}
		console.log(secondHighest);
		return secondHighest;
	}
}

// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  // Write your code here
	let freq=[];
	for(let i = 0;i < string.length;i++)
	{

		if (string[i] >= 'a' && string[i] <= 'z')
		{  
			let char = string.charAt(i);
			//console.log(freq[char]);
			if(freq[char] == undefined)
				freq[char] = 1;
			else
				freq[char] += 1;
		}
	}
	console.log(freq);
	return freq;
	 
}
let flattenObject = {};
function recursiveFlatten(unflatObject,objectStack)
{
	for(let key in unflatObject)
	{
		console.log(key + " " + unflatObject[key] + " " + typeof(unflatObject[key])) + " "+unflatObject[key].hasOwnProperty(unflatObject) + "\n";
		if(typeof(unflatObject[key]) != "object")
		{

			let keyString = "";
			for(let i = 0;i < objectStack.length;i++)
			{
				keyString += objectStack[i]+".";
			}
			keyString+=key;
			console.log("Key string is " + keyString + " value is " + unflatObject[key]);
			console.log("type of keystring is", typeof(keyString))
			flattenObject["\"" + keyString + "\""] = unflatObject[key];
			console.log(flattenObject);
		}
		else
		{
			objectStack.push(key);
			console.log(key + " pushed to stack");
			recursiveFlatten(unflatObject[key], objectStack);
		}
	}
		if(objectStack.length != 0)
			objectStack.pop();
}
// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
	let objectStack = [];
	recursiveFlatten(unflatObject, objectStack);
	console.log(flattenObject);
	// console.log(recursiveFlatten.constructor.name=="AsyncFunction");
	return flattenObject;
}

// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(flatObject) {
  let unflatObject = {}, temp;
  for(key in flatObject)
  {
	  let keyString = key.split(".");
	  temp = unflatObject;
	  for(let i = 0; i < keyString.length-1; i++)
	  {
		  if(temp[keyString[i]] == undefined)
		  {
			  if(keyString[i+1] == undefined || typeof(keyString[i+1]) == "Number")
			  	temp[keyString[i]] = [];
			  else	
			  	temp[keyString[i]] = {};
		  }
		temp = temp[keyString[i]];
	  }
	temp[keyString[keyString.length-1]] = flatObject[key];
	console.log((JSON.stringify(unflatObject)));
  }
  return unflatObject;
}
